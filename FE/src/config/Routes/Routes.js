import React from "react";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from "react-router-dom";

// * Pages yang akan di render
import {
  Login,
  Register,
  PlayerVsCom,
  P1vsP2,
  CreateRoom,
  LobbyGame,
  GameHistory,
  HomePage,
  NotFound,
  StaticPage,
  P1vsP2Closed,
} from "../../pages/AccessAllPages";
import StaticPageGame from "../../pages/StaticPageGame/StaticPageGame";

// user diharuskan untuk login
const RequiredAuth = ({ children }) => {
  const accessToken = localStorage.getItem("accessToken");
  if (accessToken === null) {
    return <Navigate to={"/login"} />;
  }
  return <>{children}</>;
};
//ketika tab ditutup item pada local storage dihapus
// window.onbeforeunload = function (event) {
//   if (event && event.type === "beforeunload") {
//     localStorage.removeItem("accessToken");
//   }
// };
function Halaman() {
  return (
    <Router>
      <Routes>
        {/* <Route path="/" element={<HomePage />}></Route>
        <Route path="/login" element={<Login />}></Route>
        <Route path="/register" element={<Register />}></Route>
        <Route path="/playervscom" element={<PlayerVsCom />}></Route>
        <Route path="/p1vsp2" element={<P1vsP2 />}></Route>
        <Route path="/lobbygame" element={<LobbyGame />}></Route>
        <Route path="/createRoom" element={<CreateRoom />}></Route>
        <Route path="/gamehistory" element={<GameHistory />}></Route>
        <Route path="*" element={<NotFound />} /> */}
        <Route path="/" element={<StaticPage />}>
          <Route path="/" element={<HomePage />}></Route>
          <Route path="/login" element={<Login />}></Route>
          <Route path="/register" element={<Register />}></Route>
        </Route>
        <Route
          path="/lobbygame"
          element={
            <RequiredAuth>
              <StaticPageGame />
            </RequiredAuth>
          }
        >
          <Route
            path="/lobbygame/playervscom"
            element={
              <RequiredAuth>
                <PlayerVsCom />
              </RequiredAuth>
            }
          ></Route>
          <Route
            path="/lobbygame/p1vsp2"
            element={
              <RequiredAuth>
                <P1vsP2 />
              </RequiredAuth>
            }
          ></Route>
          <Route
            path="/lobbygame/p1vsp2closed"
            element={
              <RequiredAuth>
                <P1vsP2Closed />
              </RequiredAuth>
            }
          ></Route>
          <Route
            path="/lobbygame"
            element={
              <RequiredAuth>
                <LobbyGame />
              </RequiredAuth>
            }
          ></Route>
          <Route
            path="/lobbygame/createRoom"
            element={
              <RequiredAuth>
                <CreateRoom />
              </RequiredAuth>
            }
          ></Route>
          <Route
            path="/lobbygame/gamehistory"
            element={
              <RequiredAuth>
                <GameHistory />
              </RequiredAuth>
            }
          ></Route>
        </Route>
        <Route path="*" element={<NotFound />} />
      </Routes>
    </Router>
  );
}

export default Halaman;
