/* eslint-disable react-hooks/rules-of-hooks */
import React, { useState, useEffect } from "react";
import rock from "../../assets/images/batu.png";
import paper from "../../assets/images/kertas.png";
import scissors from "../../assets/images/gunting.png";
import "bootstrap/dist/css/bootstrap.css";
import { useLocation } from "react-router-dom";
import "../App.css";
import { NotFound } from "../AccessAllPages";

function P1vsP2Closed() {
  const { state } = useLocation();
  // const data = location.state;
  if (state !== null) {
    console.log(state);
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [player2Opt, setplayer2Opt] = useState(state.value.player2Choice);
    const [player1Choice, setPlayer1Choice] = useState(
      state.value.player1Choice
    );
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [playerStatus, setPlayerStatus] = useState({
      player1Status: state.value.player1Status,
      player2Status: state.value.player2Status,
    });

    // decide who win the game
    const whoWin = () => {
      if (playerStatus.player1Status === playerStatus.player2Status) {
        return "DRAW";
      } else if (playerStatus.player1Status === "win" && playerStatus.player2Status === "lose") {
        return `${state.value.player1Name} WIN`;
      } else if (playerStatus.player1Status === "lose" && playerStatus.player2Status === "win") {
        return `${state.value.player2Name} WIN`;
      } else{
        return "Choose you choice"
      }
    };
    return (
      <div className="container-fluid bigContainer">
        <div className="playerVsComContainer left-container">
          <div className="playerChoiceContainer1">
            <p className="playerTitle title fs-3">{state.value.player1Name}</p>
            <div className="playerOption">
              <div
                className={
                  player1Choice === "rock" && player2Opt !== null
                    ? "choiced2"
                    : "player1choices2"
                }
              >
                <img src={rock} alt="rock" className="rock1" />
              </div>
              <div
                className={
                  player1Choice === "paper" && player2Opt !== null
                    ? "choiced2"
                    : "player1choices2"
                }
              >
                <img src={paper} alt="paper" className="paper1" />
              </div>
              <div
                className={
                  player1Choice === "scissors" && player2Opt !== null
                    ? "choiced2"
                    : "player1choices2"
                }
              >
                <img src={scissors} alt="scissors" className="scissors1" />
              </div>
            </div>
          </div>
          <div className="resultContainer">
            <h3 className="result title text-center">{whoWin()}</h3>
          </div>
          <div className="playerChoiceContainer1">
            <p className="playerTitle title fs-3 text-transform-uppercase text-center mb-0">
              {player2Opt === null
                ? "Waiting for player 2 ..."
                : `${state.value.player2Name}`}
            </p>
            <div className="playerOption">
              <div
                className={
                  player2Opt === "rock" ? "choiced2" : "player2choices2"
                }
              >
                <img src={rock} alt="rock" className="rock1" />
              </div>
              <div
                className={
                  player2Opt === "paper" ? "choiced2" : "player2choices2"
                }
              >
                <img src={paper} alt="paper" className="paper1" />
              </div>
              <div
                className={
                  player2Opt === "scissors" ? "choiced2" : "player2choices2"
                }
              >
                <img src={scissors} alt="scissors" className="scissors1" />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  } else {
    return <NotFound />;
  }
}

export default P1vsP2Closed;
