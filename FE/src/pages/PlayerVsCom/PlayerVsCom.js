import React, { useState, useEffect } from "react";
import rock from "../../assets/images/batu.png";
import paper from "../../assets/images/kertas.png";
import scissors from "../../assets/images/gunting.png";
import refresh from "../../assets/images/refresh.png";
import { Link } from "react-router-dom";
import Title from "../../components/Title";
import "../App.css";

function PlayerVsCom() {
  
  const [result, setResult] = useState("");
  const [comChoice, setComChoice] = useState("");
  const [player1Choice, setPlayer1Choice] = useState("");

  const pickRandomCom = () => {
    const pilihan = ["rock", "paper", "scissors"];
    setComChoice(pilihan[Math.floor(Math.random() * pilihan.length * 1)]);
  };

  useEffect(() => {
    if (player1Choice === "rock") {
      switch (comChoice) {
        case "paper":
          return setResult("computer win");
        case "scissors":
          return setResult(` win!`);
        default:
          return setResult("draw");
      }
    } else if (player1Choice === "paper") {
      switch (comChoice) {
        case "rock":
          return setResult(` win!`);
        case "scissors":
          return setResult("computer win");
        default:
          return setResult("draw");
      }
    } else if (player1Choice === "scissors") {
      switch (comChoice) {
        case "rock":
          return setResult("computer win");
        case "paper":
          return setResult(` win!`);
        default:
          return setResult("draw");
      }
    } else {
      setResult("");
    }
  }, [player1Choice, comChoice]);

  const setRefresh = () => {
    setPlayer1Choice("");
    setComChoice("");
  };

  const setRock = () => {
    setPlayer1Choice("rock");
    pickRandomCom();
  };
  const setPaper = () => {
    setPlayer1Choice("paper");
    pickRandomCom();
  };
  const setScissors = () => {
    setPlayer1Choice("scissors");
    pickRandomCom();
  };

  return (
    <div className="containerplayervscom">
      <div className="playerVsComContainer left-container">
        <div className="playerChoiceContainer">
          <Title classProps="playerTitle title">Player</Title>
          <Link
            className={player1Choice === "rock" ? "choiced" : "player1 choices"}
            style={
              player1Choice !== ""
                ? { pointerEvents: "none" }
                : { pointerEvents: "auto" }
            }
            onClick={setRock}
          >
            <img src={rock} alt="rock" className="rock" />
          </Link>
          <Link
            className={
              player1Choice === "paper" ? "choiced" : "player1 choices"
            }
            style={
              player1Choice !== ""
                ? { pointerEvents: "none" }
                : { pointerEvents: "auto" }
            }
            onClick={setPaper}
          >
            <img src={paper} alt="paper" className="paper" />
          </Link>
          <Link
            className={
              player1Choice === "scissors" ? "choiced" : "player1 choices"
            }
            style={
              player1Choice !== ""
                ? { pointerEvents: "none" }
                : { pointerEvents: "auto" }
            }
            onClick={setScissors}
          >
            <img src={scissors} alt="scissors" className="scissors" />
          </Link>
        </div>
        <div className="resultContainer">
          <div className="resultGame">
            <Title classProps="result title">
              {result === "" ? "Chose your choice" : `${result}`}
            </Title>
          </div>
          <div className="refreshGame" onClick={setRefresh}>
            <img src={refresh} alt="refresh" className="refreshButton" />
          </div>
        </div>
        <div className="playerChoiceContainer">
          <Title classProps="playerTitle title">COM</Title>
          <div className={comChoice === "rock" ? "choiced" : "comChoices"}>
            <img src={rock} alt="rock" className="rock" />
          </div>
          <div className={comChoice === "paper" ? "choiced" : "comChoices"}>
            <img src={paper} alt="paper" className="paper" />
          </div>
          <div className={comChoice === "scissors" ? "choiced" : "comChoices"}>
            <img src={scissors} alt="scissors" className="scissors" />
          </div>
        </div>
      </div>
    </div>
  );
}

export default PlayerVsCom;
